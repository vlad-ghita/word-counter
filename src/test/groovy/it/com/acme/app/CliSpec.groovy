package it.com.acme.app

import com.acme.app.Cli
import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Unroll

class CliSpec extends Specification {

    @Ignore
    // TODO review this test. It doesn't work properly b/c script exits during first iteration.
    @Unroll
    def "[#uid] Test illegal arguments: #args"() {
        when:
        Cli.main(args as String[])

        then:
        thrown(exceptionClass)

        where:
        uid | args           | exceptionClass
        1   | ["-file"]      | FileNotFoundException
        2   | ["--file"]     | FileNotFoundException
        3   | ["--file=bar"] | FileNotFoundException
    }

}