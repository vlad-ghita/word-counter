package it.com.acme.app

import com.acme.app.App
import java.nio.file.Paths
import spock.lang.Specification
import spock.lang.Unroll

class AppSpec extends Specification {

    @Unroll
    def "Test word count for file: #fileName"() {
        given:
        def app = new App()
        def file = getResourcesFile(fileName)


        when: "Get counters."
        def counters = app.countWordsInFile(file)

        then:
        assert counters.keySet() == refCounters.keySet()
        for (key in refCounters.keySet()) {
            assert counters[key] == refCounters[key]
        }


        when: "Get string."
        def str = app.countersToString(counters)

        then:
        str == refStr


        where:
        fileName    | refCounters                                                  | refStr
        "test1.txt" | ["foo": 2, "bar": 3, "baz": 2, "fee": 1, "Fee": 1, "123": 1] | "bar: 3\nbaz: 2\nfoo: 2\n123: 1\nFee: 1\nfee: 1"
    }

    def "Test output is identical across several files."() {
        when:
        def app = new App()

        def file1 = getResourcesFile(fileName1)
        def file2 = getResourcesFile(fileName2)

        def counters1 = app.countWordsInFile(file1)
        def counters2 = app.countWordsInFile(file2)

        def str1 = app.countersToString(counters1)
        def str2 = app.countersToString(counters2)

        then:
        str1 == str2

        where:
        fileName1   | fileName2
        "test1.txt" | "test2.txt"
    }

    def getResourcesFile(String fileName) {
        def res = getClass().getClassLoader().getResource(fileName)
        def file = Paths.get(res.toURI()).toFile()
        return file
    }

}