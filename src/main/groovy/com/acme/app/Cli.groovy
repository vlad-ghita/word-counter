package com.acme.app


import java.util.concurrent.Callable
import picocli.CommandLine
import picocli.CommandLine.Command
import picocli.CommandLine.Option

/**
 * Entry point from CLI.
 * Calls application API with relevant options.
 */
@Command(name = "word-counter", header = "Counts words in files.", mixinStandardHelpOptions = true)
class Cli implements Callable<Integer> {

    @Option(names = ["-f", "--file"], required = true, description = "File to process.") File file

    @Override
    Integer call() {
        def app = new App()
        def counters = app.countWordsInFile(file)
        def result = app.countersToString(counters)

        System.out.print(result)

        return 0
    }

    static void main(String[] args) {
        System.exit(new CommandLine(new Cli()).execute(args))
    }

}
