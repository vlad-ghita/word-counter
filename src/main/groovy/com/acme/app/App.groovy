package com.acme.app

class App {

    /**
     * Counts words in file.
     * Returns a map with word -> count.
     */
    Map<String, Integer> countWordsInFile(File file) {
        Map<String, Integer> counters = [:]

        def pattern = ~"\\w+"

        file.eachLine { line ->
            (line =~ pattern).each { String word ->
                if (counters.containsKey(word)) {
                    counters[word]++
                } else {
                    counters[word] = 1
                }
            }
        }

        return counters
    }

    /**
     * Converts counters to string.
     * Sorts by counter descending.
     * If counters are equal, sorts by word ascending.
     */
    String countersToString(Map<String, Integer> counters) {
        def result = []

        def sortedByCounterDesc = counters.sort { a, b -> b.value <=> a.value }
        def groupedByCounter = sortedByCounterDesc.groupBy { it.value }
        def groupSortedByWordAsc

        groupedByCounter.each { byCounter ->
            groupSortedByWordAsc = byCounter.value.sort { a, b -> a.key <=> b.key }

            groupSortedByWordAsc.each {
                result.add("$it.key: $it.value")
            }
        }

        return result.join("\n")
    }

}

