# Word counter

Utility to count words in files.

Reports findings in console.

### Installation

1. Download [archive](https://bitbucket.org/vlad-ghita/word-counter/src/master/dist) of choice.

1. Extract archive at desired location (named onwards `PATH_TO_INSTALLATION`).

### Usage

##### Windows helper script

1. Open Windows [command prompt](https://www.lifewire.com/how-to-open-command-prompt-2618089).

1. See tool help for usage instructions:

`<PATH_TO_INSTALLATION>\word-counter --help`

##### Run JAR

`java -jar <PATH_TO_INSTALLATION>\word-counter-jar-with-dependencies.jar --help`

### Build from sources

1. Clone repository:

`git clone git@bitbucket.org:vlad-ghita/word-counter.git`

1. Build project:

`mvn package`

1. JARs are available in `target` dir.
